import psycopg2

#establishing the connection
conn = psycopg2.connect(
   database="myDB", user='yolanda', password='a', host='127.0.0.1', port= '5432'
)
conn.autocommit = True


def createtable(name, attributes):
    cursor = conn.cursor()
    #Preparing query
    sql = 'CREATE table '+ name +  '(' + attributes + ' );'

    #Creating a table
    cursor.execute(sql)
    print("Table created successfully........")
    return


def writetable(file, name):
    cursor = conn.cursor()
    with open(file, 'r') as f:

    # skip header row
        #next(f)
        #cursor.copy_from(f, name)
        SQL = """
        COPY """+name+""" FROM STDIN WITH
            CSV
            HEADER
            DELIMITER AS ';'
        """
        cursor.copy_expert(sql=SQL, file=f)
    return


createtable('vruse', 'Land VARCHAR(50), Anteil int')
#createtable('vrdocs', 'Quelle VARCHAR(5), Autor VARCHAR(600), Titel VARCHAR(400), Jahr int, Journal VARCHAR(200), Typ VARCHAR(50), DOI VARCHAR(100), Empirisch VARCHAR(5), Kommentar VARCHAR(200), Feld VARCHAR(100), Thema VARCHAR(100), HMD VARCHAR(100), InteractionDevice VARCHAR(100)')
#createtable('meddocs', 'Author VARCHAR(2000), Titel VARCHAR(1000), Type VARCHAR(1000), AuthorKeywords VARCHAR(1000), KeywordsPlus VARCHAR(1000)')


writetable('VRUSE.csv', 'vruse')
#writetable('VRDOCS.csv', 'vrdocs')
#writetable('MEDDOCS.csv', 'meddocs')

#Closing the connection
conn.close()
