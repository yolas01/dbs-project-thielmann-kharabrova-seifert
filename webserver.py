import pandas as pd
import psycopg2
import plotly.graph_objects as go
import dash
from dash import Dash, dcc, html, Input, Output
from plotly.subplots import make_subplots



#establishing the connection to the database
conn = psycopg2.connect(
   database="myDB", user='yolanda', password='a', host='127.0.0.1', port= '5432'
)
conn.autocommit = True


def sql_to_dataframe(conn, select_query, column_names):
    cursor = conn.cursor()
    try:
        cursor.execute(select_query)
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        cursor.close()
        return 1
    tupples = cursor.fetchall()
    cursor.close()
    df = pd.DataFrame(tupples, columns=column_names)
    return df

def count(conn, name):
    cursor = conn.cursor()
    try:
        cursor.execute("""select count(*) from """+name)
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        cursor.close()
        return 1
    count = cursor.fetchall()[0][0]
    cursor.close()
    return count

# dataframe of database vruse
dfVruse = sql_to_dataframe(conn, "select * from vruse where land != 'Europe'", ["Land", "Anteil"])
dfVruse.head()


medInVR = count(conn,"""vrdocs where feld = 'Medizin'""")
numVrdocs = count(conn, 'vrdocs')
numMeddocs = count(conn, 'meddocs')





########################## creating a europe map ###############################

data = dict(type = 'choropleth',
            locations = dfVruse['Land'],
            locationmode = 'country names',
            colorscale= 'Greens',
            text= dfVruse['Land'],
            z=dfVruse['Anteil'],
            colorbar = {'title':'Country Colours', 'len':200,'lenmode':'pixels' })

layout = dict(geo = {'scope':'europe'})
map = go.Figure(data = [data],layout = layout)
map.update_layout(
    title_text='Share of clinicians using virtual reality in selected European countries in 2020 (in %)',
    title_x=0.5,
    width=1300,
    height=1300
)


# year array for dropdown
year_array=[]
for i in range (1993,2021):
    year_array.append({"label": str(i), "value": str(i)})


########################### layout with components #############################

app = dash.Dash()
app.layout = html.Div([

    html.H1("Importance of VR in medicine", style={'text-align': 'center'}),
    dcc.Graph(figure=map),
    dcc.Dropdown(id="year",
                 options=year_array,
                 multi=False,
                 value=2015,
                 style={'width': "40%"}
                 ),
    html.Div(id='output-container'),
    dcc.Graph(id='bars',figure={}),

])

########################## creating two bar graphs #############################
##################### with one responding to the dropdown ######################

@app.callback(
    Output('output-container', 'children'),
    Output(component_id='bars', component_property='figure'),
    Input('year', 'value')
)
def update_output(value):
    print(value)

    fig = make_subplots(rows=1, cols=2,
                        subplot_titles=("Number of entrys with field = medicine in VR dataset","Amount of VR literature in medical field (WOS)"))
    if not(value == None):
        fig.add_trace(go.Bar(x=['medical', 'total'], y=[count(conn, """vrdocs where feld = 'Medizin' and jahr = """+str(value)),count(conn, "vrdocs where jahr = "+str(value))]),
                      1, 1)
    else:
        fig.add_trace(go.Bar(x=['medical', 'total'], y=[medInVR,numVrdocs]),
                      1, 1)

    fig.add_trace(go.Bar(x=['VR (1993-2020)', 'total (6 days)'], y=[medInVR, numMeddocs]),
                  1, 2)

    return f'You have selected {value}', fig




app.run_server(debug=True)

conn.close()
